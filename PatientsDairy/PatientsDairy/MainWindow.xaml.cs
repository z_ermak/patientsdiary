﻿using System;
using System.Data.Entity;
using System.Windows;
using Word = Microsoft.Office.Interop.Word;
using System.IO;
using PatientsDairy.Controls;
using System.Windows.Controls;
using System.Linq;
using System.Collections.Generic;

namespace PatientsDairy
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : System.Windows.Window
    {
       
        Word._Application oWord = new Word.Application();
        private string TemplatesPath = Environment.CurrentDirectory + "\\DairyTemplates";
        private string TempPath = Environment.CurrentDirectory + "\\Temp";
        public MainWindow()
        {
            InitializeComponent();
            sldDays.Value = 6;
            LoadTemplates(TemplatesPath);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var frmEdit = new EditData();
            frmEdit.Show();
        }

        private void btnGenerate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // Считывает шаблон и сохраняет измененный в новом
                Word._Document oDoc = GetDoc(TemplatesPath + "\\Template1.dot");
                var result = "Дневник";
                string FileName;
                int i = 0;
                if (!Directory.Exists(TempPath))
                    Directory.CreateDirectory(TempPath);
                do
                {
                    i++;
                    FileName = TempPath + result + i.ToString() + ".docx";
                }
                while (File.Exists(FileName));

                oDoc.SaveAs(FileName);
                oDoc.Close();
                oDoc = null;
                System.Diagnostics.Process.Start(FileName);
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Пипец");
            }
        }

        private Word._Document GetDoc(string path)
        {
            Word._Document oDoc = oWord.Documents.Add(path);
            SetTemplate(oDoc);
            return oDoc;
        }

        private void SetTemplate(Word._Document oDoc)
        {
                foreach (DayData dayData in spnlDays.Children)
                {
                    try
                    {
                        oDoc.Bookmarks["Date" + dayData.Number.ToString()].Range.Text = dayData.Date;
                        oDoc.Bookmarks["examination" + dayData.Number.ToString()].Range.Text = dayData.TypeText;
                        oDoc.Bookmarks["PsychoStatus" + dayData.Number.ToString()].Range.Text = dayData.Text;
                    }
                    catch (Exception ex)
                    {

                    }
            }        
            // если нужно заменять другие закладки, тогда копируем верхнюю строку изменяя на нужные параметры 
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            oWord = null;
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            while (spnlDays.Children.Count > e.NewValue)
                spnlDays.Children.RemoveAt(spnlDays.Children.Count - 1);

            while (spnlDays.Children.Count < e.NewValue)
            {
                var dd = new DayData(spnlDays.Children.Count + 1);
                dd.DateChanged += OnDayDataDateChanged;
                spnlDays.Children.Add(dd);
            }
        }
        private void OnDayDataDateChanged(object sender, EventArgs e)
        {
            var dd = (DayData)sender;
            if (chkRecalculateDates.IsChecked.Value && spnlDays.Children.Count>dd.Number)
            {
                ((DayData)spnlDays.Children[dd.Number]).Date = DateTime.Parse(dd.Date).AddDays(1).ToString();
            }
        }

        private void LoadTemplates(string path)
        {
            foreach(var temp in Directory.GetFiles(path))
            {
                cmbTemplates.Items.Add(Path.GetFileName(temp));
            }
            if (cmbTemplates.Items.Count > 0)
                cmbTemplates.SelectedIndex = 0;
        }

        private void Window_MouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var rnd = new Random();
            List<string> lst = new List<string>(new[] { "Хуй", "Член", "Пенис", "Елда", "Фалос", "Писюн", "Хрен", "Уд", "Дрын"});
            MessageBox.Show(lst[rnd.Next(lst.Count - 1)], "Тыкнул?", MessageBoxButton.OK, (MessageBoxImage)(rnd.Next(4) * 16));
            
        }
    }
}
