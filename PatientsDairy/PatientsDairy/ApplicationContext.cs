﻿using System.Data.Entity;

namespace PatientsDairy
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext() : base("DefaultConnection")
        {
        }
        public DbSet<RecordType> RecordTypes { get; set; }
        public DbSet<PsychoStatus> PsychoStatuses { get; set; }
    }
}