﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Windows.Controls;

namespace PatientsDairy.Controls
{
    /// <summary>
    /// Логика взаимодействия для DayData.xaml
    /// </summary>
    public partial class DayData : UserControl
    {
        ApplicationContext db;

        public event EventHandler DateChanged;
        public string Date
        {
            get
            {
                return dtpDay.SelectedDate.Value.Date.ToString("dd.MM.yyyy");
            }
            set
            {
                dtpDay.SelectedDate = DateTime.Parse(value);
            }
        }
        public string TypeText
        {
            get
            {
                return ((RecordType)cmbRecordType.SelectedItem).Text;
            }
        }

        public string Text
        { 
            get
            {
                return ((PsychoStatus)cmbText.SelectedItem).Text;
            }
        }
        public int Number { get; set; }



        public DayData(int number)
        {
            Number = number;
            this.Name = "DayData" + Number.ToString();
            InitializeComponent();
            lblDay.Content = "День " + Number.ToString();
            dtpDay.SelectedDate = DateTime.Now;
            dtpDay.SelectedDateFormat = DatePickerFormat.Short;

            db = new ApplicationContext();
            LoadData();
            db.RecordTypes.Load();
            cmbRecordType.ItemsSource = db.RecordTypes.Local.ToBindingList();
        }

        private void LoadData()
        {
            db.PsychoStatuses.Load();

            db.RecordTypes.Load();
            cmbRecordType.ItemsSource = db.RecordTypes.Local.ToBindingList();
            cmbRecordType.SelectedIndex = 0;

            
        }

        private void cmbRecordType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int recordTypeId = ((RecordType)cmbRecordType.SelectedItem).Id;
            cmbText.ItemsSource = db.PsychoStatuses.Local.ToBindingList().Where(s=> s.RecordType == recordTypeId);
            if (cmbText.Items.Count > 0)
                cmbText.SelectedIndex = 0;
        }

        private void dayData_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            cmbRecordType.SelectedIndex = 0;
        }

        protected virtual void OnDateChanged(EventArgs e)
        {
            EventHandler handler = DateChanged;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private void dtpDay_SelectedDateChanged(object sender, SelectionChangedEventArgs e)
        {
            OnDateChanged(EventArgs.Empty);
        }
    }
}
