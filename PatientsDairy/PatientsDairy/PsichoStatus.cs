﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace PatientsDairy
{
    public class PsychoStatus : INotifyPropertyChanged
    {
        private string name;
        private string text;
        public int id { get; set; }
        public int RecordType { get; set; }

        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                OnPropertyChanged("Name");
            }
        }
        public string Text
        {
            get { return text; }
            set
            {
                text = value;
                OnPropertyChanged("Text");
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged(string prop = "")
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(prop));
        }
    }
}